#ifndef DYNBASS
#define DYNBASS

#include <SRDescent/SRDescent.h>
#include <functional>

struct DynBassInfo {
	int flags;
	int hwsize;
	int hwfree;
	int freesam;
	int free3d;
	int minrate;
	int maxrate;
	int eax;
	int minbuf;
	int dsver;
	int latency;
	int initflags;
	int speakers;
	int freq;
};

enum class DynBassConfig {
	Buffer = 0,
	UpdatePeriod,
	GVol_sample = 4,
	GVol_stream,
	GVol_music,
	Curve_vol,
	Curve_pan,
	FloatDsp,
	Algorithm3D,
	Net_timeout,
	Net_buffer,
	Pause_NoPlay,
	Net_prebuf	= 15,
	Net_passive = 18,
	Rec_buffer,
	Net_PlayList = 21,
	Music_virtual,
	Verify,
	UpdateThreads,
	Dev_buffer = 27,
	Rec_loopback,
	Vista_TruePos = 30
};

class DynBass : SRDescent {
	void *lib = nullptr;

public:
	DynBass( SRDescent *parent );
	virtual ~DynBass();

	std::function<int()>								Free;
	std::function<int( int, int, int, void *, void * )> Init;
	std::function<int()>								ErrorGetCode;

	std::function<float()>		GetVolume;
	std::function<int( float )> SetVolume;

	std::function<int()> IsStarted;
	std::function<int()> Pause;
	std::function<int()> Start;
	std::function<int()> Stop;

	std::function<int( int )> Update;

	std::function<int( DynBassInfo * )> GetInfo;

	std::function<int( DynBassConfig )>			GetConfig;
	std::function<void *( DynBassConfig )>		GetConfigPtr;
	std::function<int( DynBassConfig, int )>	SetConfig;
	std::function<int( DynBassConfig, void * )> SetConfigPtr;

	std::function<int( int, void *, long long, long long, int )> StreamCreateFile;
	std::function<int( int )>									 StreamFree;
	std::function<long long( int, int )>						 StreamGetFilePosition;

	std::function<int( int )>									 ChannelStop;
	std::function<int( int )>									 ChannelPause;
	std::function<int( int, int )>								 ChannelPlay;
	std::function<int( int, int )>								 ChannelUpdate;
	std::function<const char *( int, int )>						 ChannelGetTags;
	std::function<int( int )>									 ChannelIsActive;
	std::function<int( int, int, float * )>						 ChannelGetAttribute;
	std::function<int( int, int, float )>						 ChannelSetAttribute;
	std::function<void *( int, int, long long, void *, void * )> ChannelSetSync;

	float GameVolume();
};

#endif // DYNBASS
