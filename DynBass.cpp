#include "DynBass.h"
#include <stdexcept>
#include <string>
#include <windows.h>

DynBass::DynBass( SRDescent *parent ) : SRDescent( parent ) {
	lib = LoadLibraryA( "bass" );

	if ( !lib || lib == INVALID_HANDLE_VALUE ) throw std::runtime_error( "Can't load BASS" );

	Free		 = (int( __stdcall * )())GetProcAddress( (HMODULE)lib, "BASS_Free" );
	Init		 = (int( __stdcall * )( int, int, int, void *, void * ))GetProcAddress( (HMODULE)lib, "BASS_Init" );
	ErrorGetCode = (int( __stdcall * )())GetProcAddress( (HMODULE)lib, "BASS_ErrorGetCode" );

	GetVolume = (float( __stdcall * )())GetProcAddress( (HMODULE)lib, "BASS_GetVolume" );
	SetVolume = (int( __stdcall * )( float ))GetProcAddress( (HMODULE)lib, "BASS_SetVolume" );

	IsStarted = (int( __stdcall * )())GetProcAddress( (HMODULE)lib, "BASS_IsStarted" );
	Pause	  = (int( __stdcall * )())GetProcAddress( (HMODULE)lib, "BASS_Pause" );
	Start	  = (int( __stdcall * )())GetProcAddress( (HMODULE)lib, "BASS_Start" );
	Stop	  = (int( __stdcall * )())GetProcAddress( (HMODULE)lib, "BASS_Stop" );

	Update = (int( __stdcall * )( int ))GetProcAddress( (HMODULE)lib, "BASS_Update" );

	GetInfo = (int( __stdcall * )( DynBassInfo * ))GetProcAddress( (HMODULE)lib, "BASS_GetInfo" );

	GetConfig	 = (int( __stdcall * )( DynBassConfig ))GetProcAddress( (HMODULE)lib, "BASS_GetConfig" );
	GetConfigPtr = (void *(__stdcall *)( DynBassConfig ))GetProcAddress( (HMODULE)lib, "BASS_GetConfigPtr" );
	SetConfig	 = (int( __stdcall * )( DynBassConfig, int ))GetProcAddress( (HMODULE)lib, "BASS_SetConfig" );
	SetConfigPtr = (int( __stdcall * )( DynBassConfig, void * ))GetProcAddress( (HMODULE)lib, "BASS_SetConfigPtr" );

	StreamCreateFile = (int( __stdcall * )( int, void *, long long, long long, int ))GetProcAddress(
		(HMODULE)lib, "BASS_StreamCreateFile" );
	StreamFree = (int( __stdcall * )( int ))GetProcAddress( (HMODULE)lib, "BASS_StreamFree" );
	StreamGetFilePosition =
		(long long( __stdcall * )( int, int ))GetProcAddress( (HMODULE)lib, "BASS_StreamGetFilePosition" );

	ChannelStop		= (int( __stdcall * )( int ))GetProcAddress( (HMODULE)lib, "BASS_ChannelStop" );
	ChannelPause	= (int( __stdcall * )( int ))GetProcAddress( (HMODULE)lib, "BASS_ChannelPause" );
	ChannelPlay		= (int( __stdcall * )( int, int ))GetProcAddress( (HMODULE)lib, "BASS_ChannelPlay" );
	ChannelUpdate	= (int( __stdcall * )( int, int ))GetProcAddress( (HMODULE)lib, "BASS_ChannelUpdate" );
	ChannelGetTags	= (const char *(__stdcall *)(int, int))GetProcAddress( (HMODULE)lib, "BASS_ChannelGetTags" );
	ChannelIsActive = (int( __stdcall * )( int ))GetProcAddress( (HMODULE)lib, "BASS_ChannelIsActive" );
	ChannelGetAttribute =
		(int( __stdcall * )( int, int, float * ))GetProcAddress( (HMODULE)lib, "BASS_ChannelGetAttribute" );
	ChannelSetAttribute =
		(int( __stdcall * )( int, int, float ))GetProcAddress( (HMODULE)lib, "BASS_ChannelSetAttribute" );
	ChannelSetSync =
		(void *(__stdcall *)(int, int, long long, void *, void *))GetProcAddress( (HMODULE)lib, "BASS_ChannelSetSync" );

	//	Free();
	if ( !Init( -1, 44100, 4, 0, 0 ) ) {
		if ( ErrorGetCode() != 14 )
			throw std::runtime_error( "Can't initialize BASS. Error code " + std::to_string( ErrorGetCode() ) );
	}
	SetConfig( DynBassConfig::GVol_stream, GameVolume() * 7000.0f );
}

DynBass::~DynBass() {
	Free();
}

float DynBass::GameVolume() {
	return *(float *)0xB5FCC8;
}
